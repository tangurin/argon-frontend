import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

Vue.use(VueRouter);

class Router {

  constructor() {
    this.routesUrl = process.env.API_URL + '/frontend-api/routes';
    this.routes = [];
  }

  fetchRoutes() {
      return axios
        .get(this.routesUrl, {
          
        })
        .then(response => this.routes = response.data);
  }

  getSlug(relation) {
    return this.routes.find((route) => route.relation == relation).slug;
  }

  async buildRoutes() {
    return [
      {
        path: '',
        name: 'startpage',
        component: require(process.env.COMPONENT_PAGE_STARTPAGE).default,
      },
      {
        path: '/' + ('page') + '/:slug',
        name: 'page',
        component: () => import(/* webpackChunkName: "chunk-page" */ process.env.COMPONENT_PAGE_PAGE).then((res) => res.default),
      },
      {
        path: '/' + ('login'),
        name: 'login',
        component: () => import(/* webpackChunkName: "chunk-login" */ process.env.COMPONENT_PAGE_LOGIN).then((res) => res.default),
      },
    ]

    //........................
    //........................
    //........................

    // await this.fetchRoutes();
    // return [
    //   {
    //     path: '',
    //     name: 'startpage',
    //     component: () => require(process.env.COMPONENT_PAGE_STARTPAGE).default,
    //   },
    //   {
    //     path: '/' + this.getSlug('checkout'),
    //     name: 'checkout',
    //     component: () => import(/* webpackChunkName: "chunk-checkout" */ process.env.COMPONENT_PAGE_CHECKOUT).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('login'),
    //     name: 'login',
    //     component: () => import(/* webpackChunkName: "chunk-login" */ process.env.COMPONENT_PAGE_LOGIN).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('register'),
    //     name: 'register',
    //     component: () => import(/* webpackChunkName: "chunk-register" */ process.env.COMPONENT_PAGE_REGISTER).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('account'),
    //     name: 'account',
    //     component: () => import(/* webpackChunkName: "chunk-account" */ process.env.COMPONENT_PAGE_ACCOUNT).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('page') + '/:slug',
    //     name: 'page',
    //     component: () => import(/* webpackChunkName: "chunk-page" */ process.env.COMPONENT_PAGE_PAGE).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('category') + '/:slug',
    //     name: 'category',
    //     component: () => import(/* webpackChunkName: "chunk-category" */ process.env.COMPONENT_PAGE_CATEGORY).then((res) => res.default),
    //   },
    //   {
    //     path: '/' + this.getSlug('product') + '/:slug',
    //     name: 'product',
    //     component: () => import(/* webpackChunkName: "chunk-product" */ process.env.COMPONENT_PAGE_PRODUCT).then((res) => res.default),
    //   },
    // ];
  }

}

const createRouter = async function(ssrContext, createDefaultRouter, routerOptions) {
  const router = new Router();
  const routes =  await router.buildRoutes();

  return new VueRouter({
    mode: 'history',
    routes: routes,
  });
}

export { createRouter };

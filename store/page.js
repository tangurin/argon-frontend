export const state = () => ({
  pages: [],
});

export const getters = {
  all: (state) => state.pages,
  bySlug: (state, getters) => (slug) => getters.all.find(item => item.pageTranslation.slug == slug),
};

export const mutations = {
  pages: (state, pages) => state.pages = pages,
};

export const actions = {
  fetch ({commit}) {
    return this.$axios
      .get(process.env.API_URL + '/frontend/pages')
      .then((response) => {
        commit('pages', response.data.data);
        return response;
      });
  },
};

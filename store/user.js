export const state = () => ({
  user: {},
});

export const getters = {
  user: (state) => state.user,
  isAuthenticated: (state, getters) => getters.user.id != undefined,
  isGuest: (state, getters) => getters.isAuthenticated === false,
};

export const mutations = {
  user: (state, user) => state.user = user,
};

export const actions = {
  setUser: ({ commit }, user) => commit('user', user),
  unsetUser: ({ commit }) => commit('user', {}),

  logout ({ dispatch }) {
    return this.$axios
      .get(process.env.API_URL + '/logout')
      .then((response) => {
        dispatch('unsetUser');
        return true;
      })
      .catch((error) => false);
  },

  fetch ({ dispatch }) {
    return this.$axios
      .get(process.env.API_URL + '/user', {
        params: {
          fields: [
            'id',
            'name',
            'email',
            'customer_id',
            'active',
          ],
        }
      })
      .then((response) => {
        dispatch('setUser', response.data);
        return response;
      });
  },
};

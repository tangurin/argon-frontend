export const state = () => ({
  categories: [],
});

export const getters = {
  categories: (state) => state.categories,
  menu (state, getters) {
    return getters.categories.filter(function(item) {
        return item.active == 1 && item.show_in_menu == 1 && item.language == 'sv';
      })
      .sort((a, b) => a.sort_order > b.sort_order);
  },
};

export const mutations = {
  categories: (state, categories) => state.categories = categories,
};

export const actions = {
  fetch ({commit}) {
    return this.$axios
      .get(process.env.API_URL + '/category', {
        params: {
          fields: [
            'id',
            'active',
            'show_in_menu',
            'parent_id',
            'sort_order',
            'language',
            'name',
            'slug',
            'info',
            'meta_title',
            'meta_keywords',
            'meta_description',
          ],
        }
      })
      .then((response) => {
        commit('categories', response.data);
        return response;
      });
  },
  fetchProducts ({commit}, params) {
      return this.$axios
        .get(process.env.API_URL + '/product-node', {
          params,
        });
  },
};

export const state = () => ({
  cart: {},
  cartItems: [],
});

export const getters = {
  cart: (state) => state.cart,
  cartItems: (state) => state.cartItems,
};

export const mutations = {
  cart: (state, cart) => {
    state.cart = cart.cart
    state.cartItems = cart.cartItems
  },
};

export const actions = {
  fetch ({commit}) {
    return this.$axios
      .get(process.env.API_URL + '/cart/cart')
      .then((response) => {
        commit('cart', response.data);
        return response;
      })
      .catch(e => console.dir(e));
  },

  add ({commit, dispatch}, productData) {
    return this.$axios.post(process.env.API_URL + '/cart/add', productData)
        .then(function (response) {
            commit('cart', response.data);
            return response;
        }).catch(error => {
            throw error;
        });
  },
};

const EnvGenerator = require('../classes/env_generator');
const EnvMerge = require('../classes/env_merge');

let envGenerator = new EnvGenerator();
envGenerator.run();

let envMerge = new EnvMerge();
envMerge.run();

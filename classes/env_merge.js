const fs = require('fs');
const path = require('path');

class EnvMerge {
    constructor() {
        this.paths = [
            path.join(__dirname, '../.env-nuxt'),
            path.join(__dirname, '../storage/.env'),
        ];
    }

    readEnv (path) {
        if (fs.existsSync(path)) {
            return fs.readFileSync(path, 'utf-8');
        }
        return '';
    }

    writeEnv (fileContent) {
        const envPath = path.join(__dirname, '../.env');

        fs.closeSync(fs.openSync(envPath, 'w'));

        fs.writeFileSync(envPath, fileContent, function (err) {
        if (err) {
            throw err;
        }

          console.log(envPath + ' successfully created.');
        });
    }

    run() {
        let mergedData = '# This .env is auto generated. Any changes will not be saved.\n';
        for(let i in this.paths) {
            let path = this.paths[i];
            mergedData += '\n# ' + path + '.\n';
            mergedData += this.readEnv(path);
        }
        
        this.writeEnv(mergedData);
    }
}

module.exports = EnvMerge;

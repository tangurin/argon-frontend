const fs = require('fs');
const path = require('path');

class EnvGenerator {
    constructor() {
        this.files = [];
        this.findFiles(path.join(__dirname, '../components'));
        //this.findFiles(path.join(paths.core, 'components'));
    }

    findFiles(dir, strip) {
        strip = strip || dir;

        if (fs.existsSync(dir) === false) {
            return;
        }

        var results = [];
        var list = fs.readdirSync(dir);
        list.forEach((file) => {
            file = path.join(dir, file);
            var stat = fs.statSync(file);
            if (stat && stat.isDirectory()) {
                this.findFiles(file, strip);
            } else { 
                if (path.extname(file) == '.vue') {
                    let relativePath = file.replace(strip, '');
                    let componentName = 'COMPONENT_' + path.basename(relativePath
                            .replace(/^\/|\/$/g, '')
                            .split('/').join('_')
                            .toUpperCase()
                        , '.VUE');

                    const alreadyExists = this.files.filter((file) => file.componentName == componentName).length > 0;
                    if (alreadyExists === false) {
                        this.files.push({
                            fullPath: file,
                            relativePath: relativePath,
                            relativeFromRoot: file.replace(path.join(__dirname, '..'), ''),
                            componentName: componentName,
                        });
                    }
                }
            }
        });
        return results;
    }

    //NOT USED
    fetchRoutes() {
        const routesFile = path.join(__dirname, '../storage/routes/sv.json');
        if (fs.existsSync(routesFile) === false) {
            console.log(routesFile+ ' is missing')
            return;
        }

        try {
            let file = fs.readFileSync(routesFile);
            return JSON.parse(file);
        } catch (err) {
            console.log(err);
            return [];
        }
    }

    buildEnv(routes) {
        let fileContent = '';
        for (let i in this.files) {
            const file = this.files[i];
            fileContent += file.componentName + '="~/' + path.join('./', file.relativeFromRoot) + '"\n';
        }

        const envPath = path.join(__dirname, '../storage/.env');
        if (fs.existsSync(envPath) === false) {
            fs.closeSync(fs.openSync(envPath, 'w'));
        }

        try {
            fs.writeFileSync(envPath, fileContent, function (err) {
              if (err) throw err;
              console.log(envPath + ' successfully created.');
            });
        } catch (err) {
            console.log(err);
        }
    }

    run() {
        this.buildEnv(
            //this.fetchRoutes()
        );
    }
}

module.exports = EnvGenerator;
